package environment

import (
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"os"
)

var Config EnvConfig

type EnvConfig struct {
	AmqpDsn                          string
	QueueBackendInboundNotification  string
	QueueBackendOutboundNotification string
	DbName                           string
	MasterHost                       string
	MasterPort                       string
	MasterUser                       string
	MasterPass                       string
	ShardKey                         string
	JwtSigningKey                    string
}

func Boot() {
	if err := godotenv.Overload(".env", ".env.local"); err != nil {
		logrus.Warn("Environment file not loaded. Error: ", err.Error())
	}

	Config = EnvConfig{
		AmqpDsn:                          os.Getenv("AMQP_DSN"),
		QueueBackendInboundNotification:  os.Getenv("AMQP_QUEUE_BACKEND_INBOUND_NOTIFICATION"),
		QueueBackendOutboundNotification: os.Getenv("AMQP_QUEUE_BACKEND_OUTBOUND_NOTIFICATION"),
		DbName:                           os.Getenv("DB_NAME"),
		MasterHost:                       os.Getenv("MASTER_HOST"),
		MasterPort:                       os.Getenv("MASTER_PORT"),
		MasterUser:                       os.Getenv("MASTER_USER"),
		MasterPass:                       os.Getenv("MASTER_PASS"),
		ShardKey:                         os.Getenv("SHARD_KEY"),
		JwtSigningKey:                    os.Getenv("JWT_SIGNING_KEY"),
	}
}
