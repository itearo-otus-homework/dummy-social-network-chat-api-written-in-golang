package model

type Chat struct {
	UserId int `json:"userId" db:"user_id"`
}

type ChatMessageInput struct {
	Message       string `json:"message" binding:"required"`
	SenderId      int
	AddresseeId   int
	CorrelationId string
}

type ChatMessage struct {
	Id          int    `json:"id"          db:"id"`
	SenderId    int    `json:"senderId"    db:"sender_id"`
	AddresseeId int    `json:"addresseeId" db:"addressee_id"`
	Message     string `json:"message"     db:"message"`
}
