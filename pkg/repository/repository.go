package repository

import (
	"github.com/jmoiron/sqlx"
	"social-network-chat/pkg/repository/mysql"
)

type Repository struct {
	ChatRepository *mysql.ChatMysqlRepository
}

func NewRepository(masterDb *sqlx.DB) *Repository {
	return &Repository{
		ChatRepository: mysql.NewChatMysqlRepository(masterDb),
	}
}
