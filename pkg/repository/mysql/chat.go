package mysql

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"social-network-chat/pkg/model"
)

type ChatMysqlRepository struct {
	db *sqlx.DB
}

func NewChatMysqlRepository(db *sqlx.DB) *ChatMysqlRepository {
	return &ChatMysqlRepository{db: db}
}

type chatData struct {
	ChatId  int `db:"id"`
	ShardId int `db:"shard_id"`
}

func (r *ChatMysqlRepository) SaveNewMessage(messageInput model.ChatMessageInput) error {
	var chats []chatData
	chat, err := r.getChatByUserIds(messageInput.SenderId, messageInput.AddresseeId)
	if err == sql.ErrNoRows {
		query := "SELECT shard_id FROM " + ChatsTable + " GROUP BY shard_id ORDER BY sum(message_count)"
		err = r.db.Select(&chats, query)
		if err == sql.ErrNoRows {
			chat.ShardId = 1
		} else if err != nil {
			return err
		} else if len(chats) < ShardCount {
			chat.ShardId = len(chats) + 1
		} else {
			chat.ShardId = chats[0].ShardId
		}

		query = "INSERT INTO " + ChatsTable + " (shard_id, user_1_id, user_2_id, message_count) VALUES (?, ?, ?, ?)"
		_, err = r.db.Exec(query, chat.ShardId, messageInput.SenderId, messageInput.AddresseeId, 0)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}

	chat, err = r.getChatByUserIds(messageInput.SenderId, messageInput.AddresseeId)
	if err != nil {
		return err
	}

	// @todo: wrap two following queries in a transaction

	query := fmt.Sprintf("INSERT INTO %s (shard_id, chat_id, correlation_id, message, sender_id, addressee_id) VALUES (%d, ?, ?, ?, ?, ?)", ChatMessagesTable, chat.ShardId)
	_, err = r.db.Exec(query, chat.ShardId, messageInput.CorrelationId, messageInput.Message, messageInput.SenderId, messageInput.AddresseeId)
	if err != nil {
		return err
	}

	query = fmt.Sprintf("UPDATE %s SET message_count = message_count + 1 WHERE user_1_id = ? OR user_2_id = ? LIMIT 1", ChatsTable)
	_, err = r.db.Exec(query, messageInput.AddresseeId, messageInput.AddresseeId)

	return err
}

func (r *ChatMysqlRepository) RemoveRejectedMessage(correlationId string) error {
	logrus.Warnf("removing rejected message with correlationId '%s'", correlationId)

	_, err := r.db.Exec("DELETE FROM "+ChatMessagesTable+" WHERE correlation_id = ? LIMIT 1", correlationId)

	return err
}

func (r *ChatMysqlRepository) getChatByUserIds(user1Id int, user2Id int) (chatData, error) {
	var chat chatData
	query := fmt.Sprintf("SELECT id, shard_id FROM %s WHERE (user_1_id = ? AND user_2_id = ?) OR (user_2_id = ? AND user_1_id = ?)", ChatsTable)
	err := r.db.Get(&chat, query, user1Id, user2Id, user1Id, user2Id)

	return chat, err
}

func (r *ChatMysqlRepository) GetChatMessages(user1Id int, user2Id int) ([]model.ChatMessage, error) {
	posts := make([]model.ChatMessage, 0)

	chat, err := r.getChatByUserIds(user1Id, user2Id)
	if err == sql.ErrNoRows {
		return posts, nil
	} else if err != nil {
		return posts, err
	}

	query := fmt.Sprintf(
		"SELECT id, message, sender_id, addressee_id FROM "+ChatMessagesTable+
			" WHERE shard_id = %d AND (sender_id = ? AND addressee_id = ?) OR (addressee_id = ? AND sender_id = ?)",
		chat.ShardId,
	)
	err = r.db.Select(&posts, query, user1Id, user2Id, user1Id, user2Id)

	return posts, err
}

func (r *ChatMysqlRepository) GetChats(userId int) ([]model.Chat, error) {
	chats := make([]model.Chat, 0)

	query := fmt.Sprintf(
		"SELECT IF(user_1_id = %d, user_2_id, user_1_id) as user_id FROM "+ChatsTable+
			" WHERE user_1_id = ? OR user_2_id = ?",
		userId,
	)
	err := r.db.Select(&chats, query, userId, userId)

	return chats, err
}
