package service

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"social-network-chat/pkg/model"
	"social-network-chat/pkg/repository/mysql"
)

type ChatService struct {
	repo        *mysql.ChatMysqlRepository
	amqpService *AmqpService
}

func NewChatService(repo *mysql.ChatMysqlRepository, amqpService *AmqpService) *ChatService {
	return &ChatService{repo: repo, amqpService: amqpService}
}

func (s *ChatService) ListenForBackendOutboundNotificationEvents() error {
	events, err := s.amqpService.ConsumeBackendOutboundNotificationEvents()
	if err != nil {
		return err
	}

	type UnreadMessageCounterErrorEvent struct {
		CorrelationId string `json:"correlationId"`
	}

	var errorEvent UnreadMessageCounterErrorEvent

	for event := range events {
		logrus.Infof("received a message: %s", event.Body)

		err = json.Unmarshal(event.Body, &errorEvent)
		if err != nil {
			logrus.Error("could not deserialize event: %s", err)
		}

		_ = s.repo.RemoveRejectedMessage(errorEvent.CorrelationId)
	}

	return nil
}

func (s *ChatService) SaveNewMessage(messageInput model.ChatMessageInput) error {
	err := s.repo.SaveNewMessage(messageInput)
	if err != nil {
		return err
	}

	err = s.chatMessageAdded(ChatMessageAddedEvent{
		SenderId:      messageInput.SenderId,
		AddresseeId:   messageInput.AddresseeId,
		CorrelationId: messageInput.CorrelationId,
	})
	if err != nil {
		_ = s.repo.RemoveRejectedMessage(messageInput.CorrelationId)

		return err
	}

	return err
}

func (s *ChatService) chatMessageAdded(event ChatMessageAddedEvent) error {
	chatEvent, err := NewChatEvent("message-added", event)
	if err != nil {
		return err
	}

	return s.amqpService.PublishChatEvent(chatEvent)
}

func (s *ChatService) GetChatMessages(userId int, friendId int) ([]model.ChatMessage, error) {
	messages, err := s.repo.GetChatMessages(userId, friendId)
	if err != nil {
		return nil, err
	}

	event := ChatMessagesReadEvent{
		SenderId:    friendId,
		AddresseeId: userId,
	}

	_ = s.chatMessagesRead(event)

	return messages, err
}

func (s *ChatService) chatMessagesRead(event ChatMessagesReadEvent) error {
	chatEvent, err := NewChatEvent("messages-read", event)
	if err != nil {
		return err
	}

	return s.amqpService.PublishChatEvent(chatEvent)
}

func (s *ChatService) GetChats(userId int) ([]model.Chat, error) {
	return s.repo.GetChats(userId)
}

type ChatMessageAddedEvent struct {
	SenderId      int    `json:"senderId"`
	AddresseeId   int    `json:"addresseeId"`
	CorrelationId string `json:"correlationId"`
}

type ChatMessagesReadEvent struct {
	SenderId    int `json:"senderId"`
	AddresseeId int `json:"addresseeId"`
}
