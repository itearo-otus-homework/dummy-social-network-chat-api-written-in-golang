package service

import (
	"context"
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
	"os"
	"social-network-chat/pkg/environment"
	"time"
)

type AmqpService struct {
}

func NewAmqpService() *AmqpService {
	return &AmqpService{}
}

func (s *AmqpService) ConsumeBackendOutboundNotificationEvents() (<-chan amqp.Delivery, error) {
	amqpChannel, err := createAmqpChannel(environment.Config.QueueBackendOutboundNotification)
	if err != nil {
		return nil, err
	}

	events, err := amqpChannel.Consume(
		environment.Config.QueueBackendOutboundNotification,
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	return events, err
}

func (s *AmqpService) PublishChatEvent(event ChatEvent) error {
	amqpChannel, err := createAmqpChannel(environment.Config.QueueBackendInboundNotification)
	if err != nil {
		return err
	}

	serializedEvent, err := json.Marshal(event)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = amqpChannel.PublishWithContext(
		ctx,
		environment.Config.QueueBackendInboundNotification,
		environment.Config.QueueBackendInboundNotification,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        serializedEvent,
		},
	)

	return err
}

func createAmqpChannel(queueName string) (*amqp.Channel, error) {
	connection, err := amqp.Dial(environment.Config.AmqpDsn)
	if err != nil {
		return nil, err
	}

	channel, err := connection.Channel()
	if err != nil {
		return nil, err
	}

	err = channel.ExchangeDeclare(
		queueName,
		"direct",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}

	_, err = channel.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}

	err = channel.QueueBind(
		queueName,
		queueName,
		queueName,
		false,
		nil,
	)
	if err != nil {
		return nil, err
	}

	go func() {
		for {
			reason, ok := <-channel.NotifyClose(make(chan *amqp.Error))
			if !ok || channel.IsClosed() {
				logrus.Errorf("channel closed by the reason: %s", reason)

				_ = channel.Close() // close again, ensure closed flag set when connection closed

				os.Exit(1)
			}
		}
	}()

	return channel, nil
}

type ChatEvent struct {
	Type string
	Data json.RawMessage
}

func NewChatEvent(messageType string, event interface{}) (ChatEvent, error) {
	serializedEvent, err := json.Marshal(event)
	if err != nil {
		return ChatEvent{}, err
	}

	return ChatEvent{
		Type: messageType,
		Data: serializedEvent,
	}, nil
}
