package service

import (
	"social-network-chat/pkg/environment"
	"social-network-chat/pkg/repository"
)

type Service struct {
	*AuthService
	*ChatService
	*AmqpService
}

func NewServices(repos *repository.Repository, config environment.EnvConfig) *Service {
	amqpService := NewAmqpService()

	return &Service{
		AmqpService: amqpService,
		AuthService: NewAuthService(config.JwtSigningKey),
		ChatService: NewChatService(repos.ChatRepository, amqpService),
	}
}
