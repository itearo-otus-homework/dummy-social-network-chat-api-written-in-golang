package handler

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

const (
	authHeaderName = "Authorization"
	userIdKey      = "userId"

	correlationIdHeader = "X-Correlation-ID"
	correlationIdKey    = "correlationId"
)

func (h *Handler) apiMiddleware(c *gin.Context) {
	authHeaderValue := c.GetHeader(authHeaderName)
	if authHeaderValue == "" {
		newErrorResponse(c, http.StatusUnauthorized, "empty auth header")
		return
	}

	headerParts := strings.Split(authHeaderValue, " ")
	if len(headerParts) != 2 || headerParts[0] != "Bearer" {
		newErrorResponse(c, http.StatusUnauthorized, "invalid auth header")
		return
	}

	if len(headerParts[1]) == 0 {
		newErrorResponse(c, http.StatusUnauthorized, "token is empty")
		return
	}

	userId, err := h.services.AuthService.ParseToken(headerParts[1])
	if err != nil {
		newErrorResponse(c, http.StatusUnauthorized, err.Error())
		return
	}

	c.Set(userIdKey, userId)

	correlationId := c.GetHeader(correlationIdHeader)
	if correlationId == "" {
		correlationId = uuid.New().String()

		logrus.Info("generated new correlation id: " + correlationId)
	} else {
		logrus.Info("found header with correlation id: " + correlationId)
	}

	c.Set(correlationIdKey, correlationId)
}

func getUserId(c *gin.Context) (int, error) {
	id, ok := c.Get(userIdKey)
	if !ok {
		return 0, errors.New("user id not found")
	}

	idInt, ok := id.(int)
	if !ok {
		return 0, errors.New("user id is of invalid type")
	}

	return idInt, nil
}

func getCorrelationId(c *gin.Context) (string, error) {
	correlationIdValue, ok := c.Get(correlationIdKey)
	if !ok {
		return "", errors.New("correlation id not found")
	}

	correlationId, ok := correlationIdValue.(string)
	if !ok {
		return "", errors.New("correlation id is of invalid type")
	}

	return correlationId, nil
}
