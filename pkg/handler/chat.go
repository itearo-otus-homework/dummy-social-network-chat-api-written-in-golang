package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"social-network-chat/pkg/model"
	"strconv"
)

func (h *Handler) saveNewMessage(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	friendId, err := strconv.Atoi(c.Param("friendId"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid friendId param")
		return
	}

	var messageInput model.ChatMessageInput

	if err := c.BindJSON(&messageInput); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	messageInput.SenderId = userId
	messageInput.AddresseeId = friendId
	messageInput.CorrelationId = fmt.Sprint(uuid.New())

	err = h.services.ChatService.SaveNewMessage(messageInput)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"success": true,
	})
}

func (h *Handler) getChatMessages(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	friendId, err := strconv.Atoi(c.Param("friendId"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid friendId param")
		return
	}

	chatMessages, err := h.services.ChatService.GetChatMessages(userId, friendId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	type getChatMessagesResponse struct {
		Data []model.ChatMessage `json:"data"`
	}

	c.JSON(http.StatusOK, getChatMessagesResponse{
		Data: chatMessages,
	})
}

func (h *Handler) getChats(c *gin.Context) {
	userId, err := getUserId(c)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	chats, err := h.services.ChatService.GetChats(userId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	type getChatResponse struct {
		Data []model.Chat `json:"data"`
	}

	c.JSON(http.StatusOK, getChatResponse{
		Data: chats,
	})
}
