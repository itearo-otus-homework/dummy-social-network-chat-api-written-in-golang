package handler

import (
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func prometheusHandler() gin.HandlerFunc {
	responseTimeHistogram := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "http_server_request_duration_seconds",
		Help:    "Histogram of response time for handler in seconds",
		Buckets: prometheus.DefBuckets,
	}, []string{"route", "method", "status_code"})

	prometheus.MustRegister(responseTimeHistogram)

	return func(c *gin.Context) {
		if "/metrics" == c.Request.URL.Path {
			promhttp.Handler().ServeHTTP(c.Writer, c.Request)

			return
		}

		start := time.Now()

		c.Next()

		duration := time.Since(start)
		responseTimeHistogram.WithLabelValues(c.Request.URL.Path, c.Request.Method, strconv.Itoa(c.Writer.Status())).Observe(duration.Seconds())
	}
}
