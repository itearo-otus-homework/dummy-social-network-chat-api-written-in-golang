package handler

import (
	"github.com/gin-gonic/gin"

	"social-network-chat/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	router.Use(prometheusHandler())
	router.GET("/metrics")

	api := router.Group("/api", h.apiMiddleware)
	{
		chat := api.Group("/chat")
		{
			chat.GET("/", h.getChats)
			chat.POST("/:friendId", h.saveNewMessage)
			chat.GET("/:friendId", h.getChatMessages)
		}
	}

	return router
}
