-- +goose Up
-- +goose StatementBegin
ALTER TABLE chat_messages
    ADD COLUMN correlation_id VARCHAR(36) DEFAULT NULL AFTER id;
-- +goose StatementEnd

-- +goose StatementBegin
UPDATE chat_messages SET correlation_id = (select uuid()) WHERE correlation_id IS NULL;
-- +goose StatementEnd

-- +goose StatementBegin
ALTER TABLE chat_messages
    ALTER correlation_id DROP DEFAULT;
-- +goose StatementEnd

-- +goose StatementBegin
ALTER TABLE chat_messages
    MODIFY COLUMN correlation_id VARCHAR(36) NOT NULL;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE chat_messages
    DROP COLUMN correlation_id;
-- +goose StatementEnd
