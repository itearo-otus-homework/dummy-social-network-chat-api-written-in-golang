-- +goose Up
-- +goose StatementBegin

CREATE TABLE chats
(
    id            serial not null unique,
    user_1_id     int    not null,
    user_2_id     int    not null,
    shard_id      int    not null,
    message_count int    not null
) COLLATE utf8mb4_general_ci;

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP TABLE chats;

-- +goose StatementEnd
