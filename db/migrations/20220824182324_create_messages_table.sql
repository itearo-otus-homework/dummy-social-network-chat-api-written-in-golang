-- +goose Up
-- +goose StatementBegin

CREATE TABLE chat_messages
(
    id            serial not null unique,
    chat_id      int    not null,
    shard_id     int    not null,
    sender_id    int    not null,
    addressee_id int    not null,
    message      text   not null
) COLLATE utf8mb4_general_ci;

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP TABLE chat_messages;

-- +goose StatementEnd
