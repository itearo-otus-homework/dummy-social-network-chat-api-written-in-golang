package main

import (
	"context"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"social-network-chat/pkg/app"
	"social-network-chat/pkg/environment"
	"social-network-chat/pkg/handler"
	"social-network-chat/pkg/repository"
	"social-network-chat/pkg/repository/mysql"
	"social-network-chat/pkg/service"
	"syscall"
)

func main() {
	environment.Boot()

	logrus.Warn(environment.Config)

	masterDb, err := mysql.NewMysqlDB(mysql.Config{
		Host:     environment.Config.MasterHost,
		Port:     environment.Config.MasterPort,
		Username: environment.Config.MasterUser,
		Password: environment.Config.MasterPass,
		DBName:   environment.Config.DbName,
	})

	if err != nil {
		logrus.Fatalf("failed to initialize master db: %s", err.Error())
	}

	repos := repository.NewRepository(masterDb)
	services := service.NewServices(repos, environment.Config)
	handlers := handler.NewHandler(services)

	srv := new(app.Server)
	go func() {
		if err = srv.Run(os.Getenv("PORT"), handlers.InitRoutes()); err != nil {
			logrus.Fatalf("error occured while running http server: %s", err.Error())

			os.Exit(1)
		}
	}()

	go func() {
		if err = services.ChatService.ListenForBackendOutboundNotificationEvents(); err != nil {
			logrus.Fatalf("error occured while running amqp consumer: %s", err.Error())

			os.Exit(1)
		}
	}()

	logrus.Printf("App Started on port %s", os.Getenv("PORT"))

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("App Shutting Down")

	if err = srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

	if err = masterDb.Close(); err != nil {
		logrus.Errorf("error occured on master db connection close: %s", err.Error())
	}
}
